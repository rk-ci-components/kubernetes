# 1.0.0 (2024-04-03)


### Bug Fixes

* **inputs:** fix input indentation ([855d135](https://gitlab.com/rk-ci-components/kubernetes/commit/855d135276b7e46d23065fcd97a007e83ddffbf2))


### Features

* **release:** initial commit ([3c52974](https://gitlab.com/rk-ci-components/kubernetes/commit/3c52974968aa990830a4df97eb152c1fa1fa6c4c))
