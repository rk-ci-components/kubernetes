# GitLab CI component for Node

[![Latest Release](https://gitlab.com/rk-ci-components/node/-/badges/release.svg)](https://gitlab.com/rk-ci-components/node/-/releases)
[![pipeline status](https://gitlab.com/rk-ci-components/node/badges/master/pipeline.svg)](https://gitlab.com/rk-ci-components/node/-/commits/master)

This project implements a GitLab CI/CD template to build, test and analyse your Node projects.

## Usage

This template can be used as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration).

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  - component: gitlab.com/rk-ci-components/node/node@master
    inputs:
      eslint-disabled: true
```

## Global configuration

The semantic-release template uses some global configuration used throughout all jobs.

| Input / Variable                              | Description                  | Default value |
|-----------------------------------------------|------------------------------|---------------|
| `eslint-disabled` / `ESLINT_DISABLED`         | Disables the ESLint job.     | `false`       |
| `jest-disabled` / `JEST_DISABLED`             | Disables the Jest job.       | `false`       |
| `node-audit-disabled` / `NODE_AUDIT_DISABLED` | Disables the Node audit job. | `false`       |

## Jobs

### `eslint` job

This job performs a [ESLint](https://eslint.org/) analysis of your code.

It is bound to the `test` stage, and is **enabled by default**.

### `jest` job

This job performs [Jest](https://jestjs.io/) tests.

It is bound to the `test` stage, and is **enabled by default**.

### `node-audit` job

This job performs a vulnerability scan in your dependencies with `PKGMGR audit`.

It is bound to the `test` stage, and is **enabled by default**.

### `node-sbom` job

This job generates a [SBOM](https://cyclonedx.org/) file listing installed packages.

It is bound to the `test` stage, **enabled by default**, and uses the following variables:

| Input / Variable                       | Description                            | Default value     |
|----------------------------------------| -------------------------------------- | ----------------- |
| `sbom-disabled` / `NODE_SBOM_DISABLED` | Set to `true` to disable this job | _none_ |
| `sbom-version` / `NODE_SBOM_VERSION`   | The version of @cyclonedx used to emit SBOM | _none_ (uses latest) |
